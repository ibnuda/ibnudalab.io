Dactyl Manuform Buildlog
2019-01-12 17:11:46.466611683 UTC
Post

Ayyy!!! Whatever, mate. will write it as soon as I have the time.
Perhaps by the end of june it will be finished. I practically have no spare
time at all.

---------------------------------------------------------------------------

Ok, after a week of mind-numbingly tedious job (which is not finished yet), I
got a little time to breathe and decided to finish this blog.

Let's start with the question "why?".

# Why?

Since a couple of months ago, when I started to use Atreus, I've found a couple
"defects" in my usage.

1. The thumb keys placement. It's solely my fault, tho. I should've modified the
   thumb keys to a bit lower.
2. Tenting. Having a weird positioned desk doesn't help with my flexor. Especially
   when the desk in question is full of shit. Yeah, I'm a lazy dude who doesn't
   clean his work desk.
3. The angle. Again, because of my weird work desk, the angle doesn't really help.
   It should be more diagonal or something along that line.

So, I decided to look for split and tenting mechanical keyboard and there are a
few choice:

- Minidox. Too few keys. At least 42 like atreus.
- Ergodox / Ergodone. Too much keys. Not to mentions that those keyboards are pretty
  pricey, at least for me.
- Iris. I don't want to deal with the custom.
- Dactyl and dactyl manuform. Looks too weird but I can print it myself.

So I decided to go with the dactyl manuform because I think the keys are easier
to reach.

![Reachable by fingers](images/reachable-by-fingers.jpg)

Now, I have answered the "why?" question. Next is "how".

# How?

As usual, there are a few things that needs to be procured:

- Case. Or the plate. Or whatever float your boat. I ordered it from a cool
  dude with handle "teknogarage" at Tokopedia and Instagram (not that I use Instagram, tho).
  For a pair of 4x5 dactyl, it took almost 300 grams of print material. And the
  quality is pretty good. If you want to order it, you can send the dude an email
  at teknogarage at yahoo dot com.
- Switches. Mainly I use Hako True for the alphas and Kailh Boxen for the modifiers.
  For me, Kailh Boxen have surpassed Cherry's products. Anyway, I used 30 Hako True,
  6 Box Orange, 4 Box Blue, and 6 Box Yellow.
- Promicro clones. The keyboard itself uses two units. But because I'm a dunce,
  I've broken one unit. Well just another lesson that I've learnt.
- Wires. I've learnt from my previous build that the correct wire is breadboard's
  cable. In this build, I used approximately 6 meters of breadboard cable.
- Diodes. I can't remember the name, sorry. I have it laying around, tho.
- Keycaps. I use keycaps from my unused IKBC keyboard with OEM (or is it Cherry)
  profile. So far, or more or less 2 hours, it feels pretty fit.

## How? Again

First, I soldered the diodes to the switches. Something like the following picture.

![Soldered the diodes](images/soldered-diodes.jpg)

Followed by putting those switches to the case, hot glue them, and then wire them
using the breadboard cable like the following picture.

![Wiring the switches](images/wirings.jpg)

Now, it's the right time to flash the promicros with my own keymaps.

```
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_BASE] = LAYOUT(
  KC_SCLN, KC_COMM, KC_DOT,  KC_P, KC_Y,                                     KC_F, KC_G, KC_C,    KC_R,   KC_L,
  KC_A,    KC_O,    KC_E,    KC_U, KC_I,                                     KC_D, KC_H, KC_T,    KC_N,   KC_S,
  KC_QUOT, KC_Q,    KC_J,    KC_K, KC_X,                                     KC_B, KC_M, KC_W,    KC_V,   KC_Z,
           KC_TAB,  KC_LGUI,                                                             KC_MINS, KC_SLSH,
                                         KC_BSPC, KC_LSFT,  RAISE,   KC_SPC,
                                         KC_LALT, LOWER,    KC_ENT,  KC_LCTL,
                                         KC_DEL,  KC_ESC,   KC_LSFT, KC_LALT
),

[_RAISE] = LAYOUT(
    KC_EXLM, KC_AT,   KC_UP,   KC_LCBR, KC_RCBR,                          KC_BSLS, KC_7, KC_8,   KC_9, KC_ASTR ,
    KC_HASH, KC_LEFT, KC_DOWN, KC_RGHT, KC_DLR,                           KC_EQL,  KC_4, KC_5,   KC_6, KC_PLUS ,
    KC_LBRC, KC_RBRC, KC_LPRN, KC_RPRN, KC_AMPR,                          KC_GRV,  KC_1, KC_2,   KC_3, KC_PGUP ,
             KC_INS,  KC_LGUI,                                                     KC_0, KC_DOT,
                                               ____, ____,    ____, ____,
                                               ____, ____,    ____, ____,
                                               ____, ADJUST,  ____, ____
),
[_LOWER] = LAYOUT(
    KC_ESC,  KC_PSCR, KC_PAUS, KC_F1, KC_F2,                           KC_F3,  KC_F4,  KC_INS,  KC_DELT, KC_BSPC ,
    KC_CLCK, KC_TAB,  KC_PGUP, KC_F5, KC_F6,                           KC_F7,  KC_F8,  KC_HOME, KC_LALT, KC_ENT  ,
    KC_LSFT, KC_SLCK, KC_PGDN, KC_F9, KC_F10,                          KC_F11, KC_F12, KC_END,  KC_MINS, KC_SLSH ,
             KC_INS,  KC_LGUI,                                                         KC_APP,  ____,
                                             ____, ____,  ____, ____,
                                             ____, ____,  ____, ____,
                                             ____, ____,  ____, ____
),
[_ADJUST] = LAYOUT(
    KC_EXLM, KC_AT,   KC_UP,   KC_LCBR, KC_RCBR,                          KC_BSLS, KC_7, KC_8, KC_9,   KC_ASTR ,
    KC_HASH, KC_LEFT, KC_DOWN, KC_RGHT, KC_DLR,                           KC_EQL,  KC_4, KC_5, KC_6,   KC_PLUS ,
    ____,    ____,    ____,    ____,    RESET,                            QWERTY,  KC_1, KC_2, KC_3,   ____ ,
             ____,    ____,                                                              KC_0, KC_DOT,
                                  ____, ____,  REBASE, ____,
                                  ____, ____,  ____,   ____,
                                  ____, ____,  ____,   ____
),
[_QWERTY] = LAYOUT(
    KC_Q, KC_W, KC_E, KC_R, KC_T,                                  KC_Y, KC_U, KC_I,    KC_O,   KC_P    ,
    KC_A, KC_S, KC_D, KC_F, KC_G,                                  KC_H, KC_J, KC_K,    KC_L,   KC_SCLN ,
    KC_Z, KC_X, KC_C, KC_V, KC_B,                                  KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH ,
          ____, ____,                                                          ____,    ____,
                                KC_BSPC, KC_LSFT,  RAISE,   KC_SPC,
                                KC_LALT, LOWER,    KC_ENT,  KC_LCTL,
                                KC_DEL,  KC_ESC,   KC_LSFT, KC_LALT
),
};

```

No, I don't want to show you how ugly my final wiring. But here's the finished
product.

![Finished Dactyl](images/finished-dactyl.jpg)

# Some Commentaries

## Form
After using it for a few hours, it feels natural to use it. I don't have to move
my arms to press some keys. Now, I'm using it like a crawling snake. Really comfy, fren.

## Wiring
Wiring a dactyl manuform is a she-dog. I fucking give up if I have to wire another
dactyl.

## Switches
As I've said above, Kailh's products is comfy. Especially Hako True. I rarely bottom
them out, unlike MX Green on my another keyboard. I've misplaced a few switches, tho.
For example, I should've used Hako as the modifier too because I bottom out Box
Yellow. And the noise? Pretty much only the Box Blues which scream really loud.
Fortunately, I've designated them as not-so-frequent keys.

## Final Words
I can see myself using this keyboard at $WORK. It's pretty much silent and I can
place it in a narrow desk.
