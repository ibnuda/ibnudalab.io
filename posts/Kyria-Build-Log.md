Kyria Build Log (WIP)
2019-08-25 10:51:25.088383687 UTC
Post
Write here.

Got a package from [Thomas Baart](https://thomasbaart.nl) that contains
a set of Kyria PCB.

Preparing these pcbs!

![Kyria prep!](images/kyria-preparation.jpg)

![Kyria GET!](images/kyria-get.jpg)

Now, let me put some diodes on them.

![Kyria put some diodes!](images/kyria-diodes-attached.jpg)

![Kyria put some diodes again!](images/kyria-diodes-attached-again.jpg)

Now
